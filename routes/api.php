<?php

Route::group([
    'prefix'    => 'v1',
    'as'        => 'api.v1.',
    'namespace' => 'Api\v1'
], function () {
    Route::post('convert/word', 'WordController@convert')->name('convert.word');
});
