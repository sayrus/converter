<?php


namespace App\Services;

use Symfony\Component\DomCrawler\Crawler;

class ConvertService
{
    const EXCEPT_FILES = [
        '.gitignore'
    ];

    public function word2Html(string $fileName): string
    {
        $crawler = new Crawler(file_get_contents(storage_path('app/public/' . $fileName)));

        $body = $crawler->filter('body');
        $body->filter('img')->each(function (Crawler $node, $i) {
            $src = 'data:image/png;base64,' . base64_encode(file_get_contents(storage_path('app/public/' . $node->getNode(0)->getAttribute('src'))));
            $node->getNode(0)->setAttribute('src', $src);
        });

//        $this->clearFolder(); // TODO: Clear folder with queue

        return $body->html();

    }

    private function clearFolder()
    {
        foreach (glob(storage_path('app/public/*')) as $file) {
            if (!in_array(basename($file), self::EXCEPT_FILES))
                unlink($file);
        }
    }
}
