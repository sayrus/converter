<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\v1\WordConvertRequest;
use App\Services\ConvertService;
use GuzzleHttp\Client;
use Illuminate\Support\Str;
use NcJoes\OfficeConverter\OfficeConverter;


class WordController extends Controller
{
    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function convert(WordConvertRequest $request, ConvertService $convertService)
    {
        $file = $request->file('file');
        $fileName = Str::random('10') . '_' . time();
        $fileNameWithExt = $fileName . '.' . $file->getClientOriginalExtension();

        $file->storeAs('/', $fileNameWithExt, 'public');

        $converter = new OfficeConverter(storage_path('app/public/' . $fileNameWithExt));
        $converter->convertTo($fileName . '.html');

        return $convertService->word2Html($fileName . '.html');
    }
}
