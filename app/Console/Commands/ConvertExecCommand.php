<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ConvertExecCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'convert:exec {cmd}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $command = config('convert.command') . ' ' . base_path('convert/unoconv') . ' -f html ' . $this->argument('cmd');
        exec($command);
    }
}
